package pl.michalu.wikialistview.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "wikia_locl";
	private static final int DATABASE_VERSION = 1;

	static final String WIKIS_TABLENAME_LIST = "wikis_list";
	static final String _ID = "_id";
	static final String NAME = "name";
	static final String HUB = "hub";
	static final String LANGUAGE = "language";
	static final String TOPIC = "topic";
	static final String DOMAIN = "domain";
	static final String PAGE = "page";
	static final String ORDER = "order_f";

	static final String WIKIS_TABLENAME_DETAILS = "wikis_details";
	static final String TITLE = "title";
	static final String URL = "url";
	static final String HEADLINE = "headline";
	static final String DESC = "desc";
	static final String IMAGE = "image";
	static final String WORDMARK = "wordmark";

	private static final String DATABASE_CREATE_LIST = "create table if not exists " + WIKIS_TABLENAME_LIST + " (" + _ID
			+ " integer primary key autoincrement," + NAME + " text not null," + HUB + " text," + LANGUAGE + " text not null," + TOPIC + " text,"
			+ PAGE + " integer," + ORDER + " integer," + DOMAIN + " text not null"

			+ "); CREATE INDEX page_idx ON " + WIKIS_TABLENAME_LIST + "(" + PAGE + ");";

	private static final String DATABASE_CREATE_DETAILS = "create table if not exists " + WIKIS_TABLENAME_DETAILS + " (" + _ID
			+ " integer primary key autoincrement," + TITLE + " text not null," + URL + " text," + HEADLINE + " text," + DESC + " text not null,"
			+ LANGUAGE + " text not null," + IMAGE + " text," + WORDMARK + " text" + ");";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		database.execSQL(DATABASE_CREATE_LIST);
		database.execSQL(DATABASE_CREATE_DETAILS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}
}
