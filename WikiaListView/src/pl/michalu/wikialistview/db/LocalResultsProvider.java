package pl.michalu.wikialistview.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.michalu.wikialistview.api.model.WikiaAPIDetailsItem;
import pl.michalu.wikialistview.api.model.WikiaAPIItem;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * LocalResultsProvider includes methods to simplify communication with database
 * 
 * @author michalu
 * 
 */
public class LocalResultsProvider {
	private static final String TAG = "LocalResultsProvider";

	/**
	 * Extract locally stored wikis only from given page}
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param lang
	 *            Language chosen by user
	 * @param page
	 *            a batch to extract
	 * @return list of wikis
	 */
	public static List<WikiaAPIItem> getWikis(Context ctx, SQLiteDatabase db, String lang, String hub) {
		List<WikiaAPIItem> items = null;
		String[] args = { lang, hub };

		String selectQuery = "SELECT r._id, r.name, r.hub, r.language, r.topic, r.domain, r.page, r.order_f " + "FROM wikis_list r "
				+ "WHERE r.language = ? AND r.hub = ? " + "ORDER BY r.page ASC, r.order_f ASC";

		Cursor result = db.rawQuery(selectQuery, args);
		if (result.getCount() == 0) {
			result.close();
			return null;
		}
		if (result.getCount() != 0) {
			items = new ArrayList<WikiaAPIItem>();
			result.moveToFirst();
			while (!result.isAfterLast()) {
				items.add(new WikiaAPIItem(result.getInt(0), result.getString(1), result.getString(2), result.getString(3), result.getString(4),
						result.getString(5), result.getString(6)));
				if (!result.moveToNext())
					break;
			}
		}
		result.close();
		return items;
	}

	/**
	 * Extract all stored wikis
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param lang
	 *            Language chosen by user
	 * @return list of wikis
	 */
	public static List<WikiaAPIItem> getWikis(Context ctx, SQLiteDatabase db, String lang) {
		List<WikiaAPIItem> items = null;
		String[] args = { String.valueOf(lang) };

		String selectQuery = "SELECT r._id, r.name, r.hub, r.language, r.topic, r.domain, r.page, r.order_f " + "FROM wikis_list r "
				+ "WHERE r.language = ? " + "ORDER BY r.page ASC, r.order_f ASC";

		Cursor result = db.rawQuery(selectQuery, args);
		if (result.getCount() == 0) {
			result.close();
			return null;
		}
		if (result.getCount() != 0) {
			items = new ArrayList<WikiaAPIItem>();
			result.moveToFirst();
			while (!result.isAfterLast()) {
				items.add(new WikiaAPIItem(result.getInt(0), result.getString(1), result.getString(2), result.getString(3), result.getString(4),
						result.getString(5), result.getString(6)));
				if (!result.moveToNext())
					break;
			}
		}
		result.close();
		return items;
	}

	/**
	 * Extract all stored details of given wikis
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param lang
	 *            Language chosen by user
	 * @param keys
	 *            An array of wiki's ids
	 * @return A map of details
	 */
	public static Map<String, WikiaAPIDetailsItem> getDetails(Context ctx, SQLiteDatabase db, String lang, String[] keys) {
		Map<String, WikiaAPIDetailsItem> items = null;

		String selectQuery = "SELECT r._id, r.title, r.url, r.headline, r.language, r.desc, r.image, r.wordmark " + "FROM wikis_details r "
				+ "WHERE r.language = '" + lang + "' AND r._id IN (" + makePlaceholders(keys.length) + ")";

		Cursor result = db.rawQuery(selectQuery, keys);
		if (result.getCount() == 0) {
			result.close();
			return null;
		}
		if (result.getCount() != 0) {
			items = new HashMap<String, WikiaAPIDetailsItem>();
			result.moveToFirst();
			while (!result.isAfterLast()) {
				items.put(String.valueOf(result.getInt(0)), new WikiaAPIDetailsItem(result.getInt(0), result.getString(1), result.getString(2),
						result.getString(3), result.getString(4), result.getString(5), result.getString(6), result.getString(7)));
				if (!result.moveToNext())
					break;
			}
		}
		result.close();
		return items;
	}

	/**
	 * Delete all wikis and details from local storage
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 */
	public static void deleteAllFromLocal(Context ctx, SQLiteDatabase db) {
		try {
			int countWikis = db.delete(DatabaseHelper.WIKIS_TABLENAME_LIST, null, null);
			int countDetails = db.delete(DatabaseHelper.WIKIS_TABLENAME_DETAILS, null, null);
			Log.i(TAG, "Removed wikis: " + countWikis);
			Log.i(TAG, "Removed details: " + countDetails);
		} catch (Exception e) {
			Log.e(TAG, "Not deleted?");
		}
	}

	/**
	 * Delete wikis and details of given language from local storage
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param lang
	 *            Language chosen by user
	 */
	public static void deleteAllFromLocal(Context ctx, SQLiteDatabase db, String lang) {
		try {
			int countWikis = db.delete(DatabaseHelper.WIKIS_TABLENAME_LIST, "language = ?", new String[] { lang });
			int countDetails = db.delete(DatabaseHelper.WIKIS_TABLENAME_DETAILS, "language = ?", new String[] { lang });
			Log.i(TAG, "Removed wikis: " + countWikis);
			Log.i(TAG, "Removed details: " + countDetails);
		} catch (Exception e) {
			Log.e(TAG, "Not deleted?");
		}
	}

	/**
	 * Delete wikis of given page from local storage
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param page
	 *            a batch to delete
	 */
	public static void deleteWikisFromLocal(Context ctx, SQLiteDatabase db, int page) {
		try {
			int count = db.delete(DatabaseHelper.WIKIS_TABLENAME_LIST, "page = ?", new String[] { String.valueOf(page) });
			Log.i(TAG, "Removed: " + count);
		} catch (Exception e) {
			Log.e(TAG, "Not deleted?");
		}
	}

	/**
	 * Delete given details from local storage
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param keys
	 *            An array of details
	 */
	public static void deleteDetailsFromLocal(Context ctx, SQLiteDatabase db, String[] keys) {
		try {
			int count = db.delete(DatabaseHelper.WIKIS_TABLENAME_DETAILS, "_id IN (" + makePlaceholders(keys.length) + ")",
					keys);
			Log.i(TAG, "Removed details: " + count);
		} catch (Exception e) {
			Log.e(TAG, "Not deleted?");
		}
	}

	/**
	 * Check if any wikis exist in local storage
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @return result of checking for data in db
	 */
	public static boolean isAnybodyHere(Context ctx, SQLiteDatabase db) {
		boolean result = false;
		String selectQuery = "SELECT count(*) FROM " + DatabaseHelper.WIKIS_TABLENAME_LIST;
		Cursor c = db.rawQuery(selectQuery, null);
		if (c.moveToFirst() && c.getInt(0) > 0)
			result = true;
		c.close();
		return result;
	}

	/**
	 * Make a string of '?' (replaced later for data from an array) suitable for
	 * WHERE ... IN statement
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param len
	 *            length of array of parameters
	 */
	public static String makePlaceholders(int len) {
		if (len < 1) {
			throw new RuntimeException("No placeholders");
		} else {
			StringBuilder sb = new StringBuilder(len * 2 - 1);
			sb.append("?");
			for (int i = 1; i < len; i++) {
				sb.append(",?");
			}
			return sb.toString();
		}
	}

	/**
	 * Stores downloaded wikis from Wikia API into local database
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param items
	 *            a list of wikis downloaded from Wikia API
	 * @param page
	 *            indicate which batch of data it is
	 */
	public static void insertWikis(Context ctx, SQLiteDatabase db, List<WikiaAPIItem> items, int page) {
		deleteWikisFromLocal(ctx, db, page);
		for (WikiaAPIItem item : items) {
			ContentValues cv = new ContentValues(8);
			cv.put(DatabaseHelper._ID, item.getId());
			cv.put(DatabaseHelper.NAME, item.getName());
			cv.put(DatabaseHelper.HUB, item.getHub());
			cv.put(DatabaseHelper.TOPIC, item.getTopic());
			cv.put(DatabaseHelper.DOMAIN, item.getDomain());
			cv.put(DatabaseHelper.LANGUAGE, item.getLanguage());
			cv.put(DatabaseHelper.PAGE, page);
			cv.put(DatabaseHelper.ORDER, items.indexOf(item));
			try {
				db.insertOrThrow(DatabaseHelper.WIKIS_TABLENAME_LIST, null, cv);
			} catch (SQLiteConstraintException e) {
				continue;
			}
		}
	}

	/**
	 * Stores downloaded details from Wikia API into local database
	 * 
	 * @param ctx
	 *            Context which allows access to application-specific resources
	 *            and classes
	 * @param db
	 *            Local SQLite database
	 * @param map
	 *            a map of downloaded details objects
	 */
	public static void insertDetails(Context ctx, SQLiteDatabase db, Map<String, WikiaAPIDetailsItem> map) {
		deleteDetailsFromLocal(ctx, db, map.keySet().toArray(new String[0]));
		for (Map.Entry<String, WikiaAPIDetailsItem> item : map.entrySet()) {
			ContentValues cv = new ContentValues(8);
			WikiaAPIDetailsItem detail = item.getValue();

			cv.put(DatabaseHelper._ID, detail.getId());
			cv.put(DatabaseHelper.TITLE, detail.getTitle());
			cv.put(DatabaseHelper.URL, detail.getUrl());
			cv.put(DatabaseHelper.HEADLINE, detail.getHeadline());
			cv.put(DatabaseHelper.LANGUAGE, detail.getLang());
			cv.put(DatabaseHelper.DESC, detail.getDesc());
			cv.put(DatabaseHelper.IMAGE, detail.getImage());
			cv.put(DatabaseHelper.WORDMARK, detail.getWordmark());

			try {
				db.insertOrThrow(DatabaseHelper.WIKIS_TABLENAME_DETAILS, null, cv);
			} catch (SQLiteConstraintException e) {
				continue;
			}
		}
	}

}
