package pl.michalu.wikialistview.base;

import pl.michalu.wikialistview.R;
import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

public class WikiaApp extends Application {
	@Override
	public void onCreate() {
		super.onCreate();

		// configure options for displaying particular images in imageViews
		DisplayImageOptions displayimageOptions = new DisplayImageOptions.Builder().displayer(new SimpleBitmapDisplayer())
				.resetViewBeforeLoading(true).showImageOnLoading(R.drawable.photo).cacheInMemory(true).cacheOnDisc(true).build();

		// global configuration
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.defaultDisplayImageOptions(displayimageOptions).discCacheFileCount(500).build();

		ImageLoader.getInstance().init(config);
	}
}
