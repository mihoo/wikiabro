package pl.michalu.wikialistview.api.model;

public class WikiaAPIItem {

	private int id;
	private String name;
	private String hub;
	private String language;
	private String topic;
	private String domain;
	private String order;

	public WikiaAPIItem(int id, String name, String hub, String language, String topic, String domain, String order) {
		super();
		this.id = id;
		this.name = name;
		this.hub = hub;
		this.language = language;
		this.topic = topic;
		this.domain = domain;
		this.order = order;

	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return ((WikiaAPIItem) o).id == this.id;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}
