package pl.michalu.wikialistview.api.model;

import java.util.List;

public class WikiaAPIRoot {
	private int next;
	private int total;
	private int batches;
	private int currentBatch;
	private List<WikiaAPIItem> items;

	public WikiaAPIRoot(int next, int total, int batches, int currentBatch,
			List<WikiaAPIItem> items) {
		super();
		this.next = next;
		this.total = total;
		this.batches = batches;
		this.currentBatch = currentBatch;
		this.items = items;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getBatches() {
		return batches;
	}

	public void setBatches(int batches) {
		this.batches = batches;
	}

	public int getCurrentBatch() {
		return currentBatch;
	}

	public void setCurrentBatch(int currentBatch) {
		this.currentBatch = currentBatch;
	}

	public List<WikiaAPIItem> getItems() {
		return items;
	}

	public void setItems(List<WikiaAPIItem> items) {
		this.items = items;
	}

}
