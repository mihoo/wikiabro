package pl.michalu.wikialistview.api.model;

import java.util.Map;

public class WikiaAPIDetailsRoot {
	Map<String, WikiaAPIDetailsItem> items;

	public Map<String, WikiaAPIDetailsItem> getItems() {
		return items;
	}

	public void setItems(Map<String, WikiaAPIDetailsItem> items) {
		this.items = items;
	}

}
