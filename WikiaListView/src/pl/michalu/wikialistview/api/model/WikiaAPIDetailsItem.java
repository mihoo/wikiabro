package pl.michalu.wikialistview.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class represents details of wiki returned by Wikia API
 * @author michalu
 *
 */
public class WikiaAPIDetailsItem implements Parcelable {
	private int id;
	private String title;
	private String url;
	private String headline;
	private String lang;
	private String desc;
	private String image;
	private String wordmark;

	public WikiaAPIDetailsItem() {
		// TODO Auto-generated constructor stub
	}

	public WikiaAPIDetailsItem(int id, String title, String url, String headline, String lang, String desc, String image, String wordmark) {
		super();
		this.id = id;
		this.title = title;
		this.url = url;
		this.headline = headline;
		this.lang = lang;
		this.desc = desc;
		this.image = image;
		this.wordmark = wordmark;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getWordmark() {
		return wordmark;
	}

	public void setWordmark(String wordmark) {
		this.wordmark = wordmark;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(title);
		dest.writeString(headline);
		dest.writeString(desc);
		dest.writeString(image);
		dest.writeString(url);
		dest.writeString(wordmark);

	}

	public static final Parcelable.Creator<WikiaAPIDetailsItem> CREATOR = new Creator<WikiaAPIDetailsItem>() {
		
		public WikiaAPIDetailsItem createFromParcel(Parcel source) {
			WikiaAPIDetailsItem item = new WikiaAPIDetailsItem();
			item.id = source.readInt();
			item.title = source.readString();
			item.headline = source.readString();
			item.desc = source.readString();
			item.image = source.readString();
			item.url = source.readString();
			item.wordmark = source.readString();
			return item;
		}

		public WikiaAPIDetailsItem[] newArray(int size) {
			return new WikiaAPIDetailsItem[size];
		}

	};

}
