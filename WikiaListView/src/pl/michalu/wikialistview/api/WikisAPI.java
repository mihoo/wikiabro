package pl.michalu.wikialistview.api;

import pl.michalu.wikialistview.R;
import pl.michalu.wikialistview.api.model.WikiaAPIRoot;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Class to handle Wikia API and to store some data locally in SharedPreferences
 * @author michalu
 *
 */
public class WikisAPI {

	public static final String API_PREFS_FILE = "api_values";
	private static final String API_TOTAL = "total";
	private static final String API_NEXT = "next";
	private static final String API_CURRENT = "current";
	private static final String API_LIMIT = "limit";
	private static String GET_LIST_BASE = "http://www.wikia.com/api/v1/Wikis/List/?";
	private static String GET_DETAILS_BASE = "http://www.wikia.com/api/v1/Wikis/Details/?";
	private static int THUMB_WIDTH = 200;
	private static int THUMB_HEIGHT = 200;

	private static WikisAPI apiInstance;
	private Context ctx;

	private WikisAPI(Context ctx) {
		this.ctx = ctx;
	}

	public static synchronized WikisAPI getInstance(Context ctx) {
		if (apiInstance == null) {
			apiInstance = new WikisAPI(ctx);
		}
		return apiInstance;
	}

	public void update(WikiaAPIRoot root) {
		setNext(root.getNext());
		setTotal(root.getTotal());
		setCurrentBatch(root.getCurrentBatch());

	}

	public void clear() {
		setNext(25);
		setTotal(250);
		setCurrentBatch(0);
	}

	public synchronized String getGetListUrl() {
		String result = null;
		if (getNext() > 0) {
			StringBuilder sb = new StringBuilder(GET_LIST_BASE);
			sb.append("lang=" + getLanguage());
			sb.append("&limit=" + getLimit());
			sb.append("&hub=" + getHub());
			int current = getCurrentBatch();
			if (current == 0) {
				setCurrentBatch(1);
				sb.append("&batch=" + getCurrentBatch());
			} else {
				setCurrentBatch(current + 1);
				sb.append("&batch=" + getCurrentBatch());
			}
			result = sb.toString();
		}
		return result;
	}

	public synchronized String getGetDetailsUrl(int wikiId) {
		String result = null;
		StringBuilder sb = new StringBuilder(GET_DETAILS_BASE);
		sb.append("&ids=" + wikiId);
		sb.append("&height=" + THUMB_HEIGHT + "&width=" + THUMB_WIDTH);
		result = sb.toString();
		return result;
	}

	public synchronized String getGetDetailsUrl(String[] wikiIds) {
		String result = null;
		if (wikiIds.length > 0) {
			StringBuilder sb = new StringBuilder(GET_DETAILS_BASE);
			sb.append("&ids=");
			for (int i = 0; i < wikiIds.length; i++) {
				if (i == wikiIds.length - 1) {
					sb.append(wikiIds[i]);
				} else
					sb.append(wikiIds[i] + ",");
			}
			sb.append("&height=" + THUMB_HEIGHT + "&width=" + THUMB_WIDTH);
			result = sb.toString();
		}
		return result;
	}

	public int getNext() {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		return prefs.getInt(API_NEXT, 25);
	}

	public void setNext(int next) {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt(API_NEXT, next);
		editor.commit();
	}

	public int getTotal() {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		return prefs.getInt(API_TOTAL, Integer.MAX_VALUE);
	}

	public void setTotal(int total) {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt(API_TOTAL, total);
		editor.commit();
	}

	public int getCurrentBatch() {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		return prefs.getInt(API_CURRENT, 0);
	}

	public void setCurrentBatch(int currentBatch) {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt(API_CURRENT, currentBatch);
		editor.commit();
	}

	public int getLimit() {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		return prefs.getInt(API_LIMIT, 25);
	}

	public void setLimit(int limit) {
		SharedPreferences prefs = ctx.getSharedPreferences(WikisAPI.API_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		editor.putInt(API_LIMIT, limit);
		editor.commit();
	}

	public String getLanguage() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		String lang = prefs.getString("pref_language", ctx.getString(R.string.pref_language_en));
		return lang;
	}
	
	public String getHub() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		String lang = prefs.getString("pref_hub", ctx.getString(R.string.pref_hub_default));
		return lang;
	}
}
