package pl.michalu.wikialistview.fragment;

import pl.michalu.wikialistview.R;
import pl.michalu.wikialistview.activity.WikiaActivity;
import pl.michalu.wikialistview.api.model.WikiaAPIDetailsItem;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Fragment shows details about clicked wiki on the list Fragment displays
 * wordmark (if available), description (if available), user can share content
 * via intent and go to the external wiki website
 * 
 * @author michalu
 * 
 */
public class WikisDetails extends Fragment {

	private WikiaAPIDetailsItem details;
	private ShareActionProvider mShareActionProvider;

	public WikisDetails() {
		setHasOptionsMenu(true);
	}

	public static WikisDetails create(WikiaAPIDetailsItem details) {
		WikisDetails frag = new WikisDetails();
		Bundle extra = new Bundle();
		extra.putParcelable("details", details);
		frag.setArguments(extra);
		return frag;
	}

	// states
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle extras = getArguments();
		if (extras != null && extras.containsKey("details")) {
			details = extras.getParcelable("details");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.details_view, container, false);
		final ImageView image = (ImageView) v.findViewById(R.id.details_image);
		final TextView desc = (TextView) v.findViewById(R.id.details_desc);
		if (details.getImage() != null)
			ImageLoader.getInstance().displayImage(details.getWordmark(), image);
		else
			image.setVisibility(View.GONE);
		String description = details.getDesc();
		if (description != null)
			desc.setText(description);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if (details.getTitle() != null) {
			((WikiaActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
			((WikiaActivity) getActivity()).getSupportActionBar().setTitle(details.getTitle());
			((WikiaActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
			((WikiaActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	// menu
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.details_menu, menu);
		if (details.getUrl() != null)
			menu.findItem(R.id.actionbar_details_go).setVisible(true);
		// Locate MenuItem with ShareActionProvider
		MenuItem item = menu.findItem(R.id.menu_item_share);
		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
		mShareActionProvider.setShareIntent(getIntent());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			getFragmentManager().popBackStack();
			return true;
		case R.id.actionbar_details_go:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(details.getUrl()));
			startActivity(browserIntent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	// Call to update the share intent
	private Intent getIntent() {
		Intent sharingIntent = null;
		if (mShareActionProvider != null) {
			sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			String desc = getString(R.string.details_share_visit) + " " + details.getHeadline() + ": " + details.getUrl() + " " + details.getDesc();
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, details.getHeadline());
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, desc);
			mShareActionProvider.setShareIntent(sharingIntent);
		}
		return sharingIntent;
	}

}
