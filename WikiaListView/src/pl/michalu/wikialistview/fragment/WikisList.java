package pl.michalu.wikialistview.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.michalu.wikialistview.R;
import pl.michalu.wikialistview.activity.About;
import pl.michalu.wikialistview.activity.SettingsActivity;
import pl.michalu.wikialistview.activity.WikiaActivity;
import pl.michalu.wikialistview.api.WikisAPI;
import pl.michalu.wikialistview.api.model.WikiaAPIDetailsItem;
import pl.michalu.wikialistview.api.model.WikiaAPIItem;
import pl.michalu.wikialistview.api.model.WikiaAPIRoot;
import pl.michalu.wikialistview.db.DatabaseHelper;
import pl.michalu.wikialistview.db.LocalResultsProvider;
import pl.michalu.wikialistview.net.NetworkManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.rits.cloning.Cloner;

/**
 * A fragment containing a list of wikis
 * 
 * @author michalu
 * 
 */
public class WikisList extends Fragment {
	private List<WikiaAPIItem> items = new ArrayList<WikiaAPIItem>();
	private List<WikiaAPIItem> itemsOriginal = new ArrayList<WikiaAPIItem>();
	private Map<String, WikiaAPIDetailsItem> details = new HashMap<String, WikiaAPIDetailsItem>();
	private EfficientAdapter eAdapter;
	private LoadMoreListView listView;
	private ProgressBar progress;
	private TextView message;
	private WikisAPI api;
	private SharedPreferences prefs;

	public static WikisList create(int detailsContainerId) {
		WikisList fragment = new WikisList();
		return fragment;
	}

	public WikisList() {
		setRetainInstance(true);
		setHasOptionsMenu(true);
	}

	// states

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if (eAdapter == null) {
			eAdapter = new EfficientAdapter(getActivity());
		}
		api = WikisAPI.getInstance(getActivity());
		api.clear();
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		prefs.registerOnSharedPreferenceChangeListener(spListener);
		new GetWikisListTask().execute();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.list_fragment, container, false);
		listView = (LoadMoreListView) v.findViewById(R.id.list);
		listView.setAdapter(eAdapter);
		listView.setTextFilterEnabled(true);
		progress = (ProgressBar) v.findViewById(R.id.list_progress);
		message = (TextView) v.findViewById(R.id.list_message);
		message.setOnClickListener(messageListener);
		checkForInternetOrData(getActivity());
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		listView.setOnLoadMoreListener(onLoadMoreWikisListener);
		listView.setOnItemClickListener(onItemClickListener);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		if (prefs.getBoolean("pref_clear_cache", false)) {
			removeAllLocally(getActivity(), prefs);
			new GetWikisListTask().execute();
		}
		((WikiaActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.wikia_label) + ":" + api.getHub());
		((WikiaActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		if (items.size() > 0) {
			progress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			message.setVisibility(View.GONE);
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	// adapter

	private class EfficientAdapter extends BaseAdapter implements Filterable {
		private LayoutInflater mInflater;

		public EfficientAdapter(Context context) {
			// Cache the LayoutInflate to avoid asking for a new one each time.
			mInflater = LayoutInflater.from(context);
		}

		/**
		 * The number of items in the list is determined by the number of hotels
		 * in our array.
		 * 
		 * @see android.widget.ListAdapter#getCount()
		 */
		public int getCount() {
			if (items != null)
				return items.size();
			return 0;
		}

		public Object getItem(int position) {
			return position;
		}

		/**
		 * Use the array index as a unique id.
		 * 
		 * @see android.widget.ListAdapter#getItemId(int)
		 */
		public long getItemId(int position) {
			return position;
		}

		/**
		 * Make a view to hold each row.
		 * 
		 * @see android.widget.ListAdapter#getView(int, android.view.View,
		 *      android.view.ViewGroup)
		 */
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.list_item, null);
				holder = new ViewHolder();
				holder.title = (TextView) convertView.findViewById(R.id.list_wiki_title);
				holder.thumb = (ImageView) convertView.findViewById(R.id.list_wiki_thumb);
				holder.domain = (TextView) convertView.findViewById(R.id.list_wiki_url);

				convertView.setTag(holder);
			} else {
				// Get the ViewHolder back to get fast access to the TextView
				// and the ImageView.
				holder = (ViewHolder) convertView.getTag();
			}

			WikiaAPIItem item = items.get(position);

			holder.title.setText(item.getName());
			holder.domain.setText(item.getDomain());
			if (details != null && details.containsKey(String.valueOf(item.getId()))) {
				WikiaAPIDetailsItem detail = details.get(String.valueOf(item.getId()));
				ImageLoader.getInstance().displayImage(detail.getImage(), holder.thumb);
			}

			return convertView;
		}

		class ViewHolder {
			TextView title;
			TextView domain;
			ImageView thumb;
		}

		@Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			Filter filter = new Filter() {

				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					// TODO Auto-generated method stub
					FilterResults results = new FilterResults();
					ArrayList<WikiaAPIItem> filteredAdapter = new ArrayList<WikiaAPIItem>();
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < items.size(); i++) {
						String dataNames = items.get(i).getName();
						if (dataNames.toLowerCase().contains(constraint.toString())) {
							filteredAdapter.add(items.get(i));
						}
					}
					results.count = filteredAdapter.size();
					results.values = filteredAdapter;
					return results;
				}

				@SuppressWarnings("unchecked")
				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					// TODO Auto-generated method stub
					items = (List<WikiaAPIItem>) results.values;
					notifyDataSetChanged();
				}

			};
			return filter;
		}
	};

	// menu

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.actionbar_preferences, menu);
		MenuItem searchItem = menu.findItem(R.id.actionbar_searchview);
		SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				if (query != null && query.trim().length() >= 3) {
					eAdapter.getFilter().filter(query.trim());
					return true;
				} else {
					items = new Cloner().deepClone(itemsOriginal);
					eAdapter.notifyDataSetChanged();
				}
				return false;
			}

			@Override
			public boolean onQueryTextChange(String query) {
				// TODO Auto-generated method stub
				if (query != null && query.trim().length() >= 3) {
					eAdapter.getFilter().filter(query.trim());
					return true;
				} else {
					items = new Cloner().deepClone(itemsOriginal);
					eAdapter.notifyDataSetChanged();
				}
				return false;
			}
		});
		MenuItemCompat.setOnActionExpandListener(searchItem, new OnActionExpandListener() {
			@Override
			public boolean onMenuItemActionCollapse(MenuItem item) {
				// Do something when collapsed
				items = new Cloner().deepClone(itemsOriginal);
				eAdapter.notifyDataSetChanged();
				return true; // Return true to collapse action view
			}

			@Override
			public boolean onMenuItemActionExpand(MenuItem item) {
				// Do something when expanded
				return true; // Return true to expand action view
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.actionbar_menu_settings:
			// Display the fragment as the main content.
			Intent prefs = new Intent(getActivity(), SettingsActivity.class);
			startActivity(prefs);
			return true;
		case R.id.actionbar_menu_about:
			Intent about = new Intent(getActivity(), About.class);
			startActivity(about);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Listeners

	OnClickListener messageListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (NetworkManager.isOnline(getActivity())) {
				new GetWikisListTask().execute();
			} else
				Toast.makeText(getActivity(), getString(R.string.no_internet_toast), Toast.LENGTH_SHORT).show();
		}
	};

	SharedPreferences.OnSharedPreferenceChangeListener spListener = new OnSharedPreferenceChangeListener() {
		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
			// TODO Auto-generated method stub
			if (key.equals("pref_language") || key.equals("pref_hub")) {
				resetView();
				new GetWikisListTask().execute();
			}
		}
	};

	OnLoadMoreListener onLoadMoreWikisListener = new OnLoadMoreListener() {
		@Override
		public void onLoadMore() {
			// TODO Auto-generated method stub
			new GetWikisListTask().execute();
		}
	};

	OnItemClickListener onItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			// TODO Auto-generated method stub
			if (position >= items.size())
				return;
			WikiaAPIItem item = items.get(position);
			WikiaAPIDetailsItem detail = details.get(String.valueOf(item.getId()));

			if (detail != null) {
				WikisDetails fragment = WikisDetails.create(detail);
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.wikis_list_view, fragment);
				ft.addToBackStack(null);
				ft.commit();
				getFragmentManager().executePendingTransactions();
			}

			items = new Cloner().deepClone(itemsOriginal);
		}
	};

	// asynctasks

	/**
	 * This asynctask downloads a part of wikis from Wikia API and store results
	 * in local storage (sqlite) for offline browsing. After successfully
	 * downloading a part of results starts a GetWikiDetailsTask and refreshes
	 * listView
	 * 
	 * @author michalu
	 * 
	 */

	private class GetWikisListTask extends AsyncTask<String, Void, List<WikiaAPIItem>> {
		@Override
		protected List<WikiaAPIItem> doInBackground(String... params) {
			List<WikiaAPIItem> result = null;
			if (isCancelled()) {
				return null;
			}
			if (items.size() < api.getTotal())
				try {
					if (NetworkManager.isOnline(getActivity())) {
						String url = api.getGetListUrl();
						String json = NetworkManager.downloadUrl(getActivity(), url);
						WikiaAPIRoot rootAPI = new Gson().fromJson(json, WikiaAPIRoot.class);
						if (rootAPI != null) {
							result = rootAPI.getItems();
							saveResultsLocally(result, api.getCurrentBatch());
							api.update(rootAPI);
						}

					} else if (items.size() == 0) {
						result = getDataLocally();
					}

				} catch (IOException e) {
					return result;
				}
			return result;
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(List<WikiaAPIItem> result) {
			if (result != null) {
				List<WikiaAPIItem> tempResults = result;

				String[] ids = new String[tempResults.size()];
				for (int i = 0; i < tempResults.size(); i++) {
					ids[i] = String.valueOf(tempResults.get(i).getId());
				}

				new GetWikiDetailsTask().execute(ids);

				for (WikiaAPIItem item : tempResults)
					if (!items.contains(item))
						items.add(item);
				// We need notify the adapter that the data have been changed
				eAdapter.notifyDataSetChanged();
				// Call onLoadMoreComplete when the LoadMore task, has finished
				if (!listView.isShown()) {
					progress.setVisibility(View.GONE);
					message.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
				}

			}
			if (items.size() == 0 && !NetworkManager.isOnline(getActivity())) {
				progress.setVisibility(View.GONE);
				message.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
			}
			listView.onLoadMoreComplete();
			itemsOriginal = new Cloner().deepClone(items);
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
			// Notify the loading more operation has finished
			listView.onLoadMoreComplete();
		}
	}

	/**
	 * GetWikiDetailsTask downloads details about given wikis ids using Wikia
	 * API and stores it locally (sqlite) for offline browsing
	 * 
	 * @author michalu
	 * 
	 */

	private class GetWikiDetailsTask extends AsyncTask<String, Void, Map<String, WikiaAPIDetailsItem>> {
		@Override
		protected Map<String, WikiaAPIDetailsItem> doInBackground(String... params) {
			Map<String, WikiaAPIDetailsItem> result = null;
			if (params.length <= 0)
				return null;
			if (isCancelled()) {
				return null;
			}
			if (NetworkManager.isOnline(getActivity())) {
				try {
					String url = api.getGetDetailsUrl(params);
					String json = NetworkManager.downloadUrl(getActivity(), url);
					JsonParser jsonParser = new JsonParser();
					JsonElement element = jsonParser.parse(json);
					if (element != null) {
						JsonObject object = element.getAsJsonObject();

						JsonObject itemsObject = object.getAsJsonObject("items");
						result = new Gson().fromJson(itemsObject, new TypeToken<Map<String, WikiaAPIDetailsItem>>() {
						}.getType());

						saveResultsLocally(result);
					}

				} catch (IOException e) {
					return null;
				}
			} else {
				result = getDataLocally(params);
			}
			return result;
		}

		@Override
		protected void onPostExecute(Map<String, WikiaAPIDetailsItem> result) {
			if (result != null) {
				details.putAll(result);
				eAdapter.notifyDataSetChanged();
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
		}
	}

	// other useful methods

	// check for Internet connection and for any locally stored data
	private void checkForInternetOrData(Context ctx) {
		DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		if (!NetworkManager.isOnline(ctx) && !LocalResultsProvider.isAnybodyHere(ctx, db)) {
			progress.setVisibility(View.GONE);
			listView.setVisibility(View.GONE);
			message.setVisibility(View.VISIBLE);
		}
		db.close();
	}

	// get list of wikis from sqlite db
	private List<WikiaAPIItem> getDataLocally() {
		DatabaseHelper dbHelper;
		SQLiteDatabase db;
		List<WikiaAPIItem> result = null;
		if (getActivity() != null) {
			dbHelper = new DatabaseHelper(getActivity());
			db = dbHelper.getReadableDatabase();
			result = LocalResultsProvider.getWikis(getActivity(), db, api.getLanguage(), api.getHub());
			db.close();
		}
		return result;
	}

	// get a map of wikis details from sqlite db
	private Map<String, WikiaAPIDetailsItem> getDataLocally(String[] keys) {
		DatabaseHelper dbHelper;
		SQLiteDatabase db;
		Map<String, WikiaAPIDetailsItem> result = null;
		if (getActivity() != null) {
			dbHelper = new DatabaseHelper(getActivity());
			db = dbHelper.getReadableDatabase();
			result = LocalResultsProvider.getDetails(getActivity(), db, api.getLanguage(), keys);
			db.close();
		}
		return result;
	}

	// saves list of wikis locally
	private void saveResultsLocally(List<WikiaAPIItem> items, int page) {
		DatabaseHelper dbHelper;
		SQLiteDatabase db;
		if (getActivity() != null) {
			dbHelper = new DatabaseHelper(getActivity());
			db = dbHelper.getWritableDatabase();
			LocalResultsProvider.insertWikis(getActivity(), db, items, page);
			db.close();
		}
	}

	// saves a map of details locally
	private void saveResultsLocally(Map<String, WikiaAPIDetailsItem> map) {
		DatabaseHelper dbHelper;
		SQLiteDatabase db;
		if (getActivity() != null) {
			dbHelper = new DatabaseHelper(getActivity());
			db = dbHelper.getWritableDatabase();
			LocalResultsProvider.insertDetails(getActivity(), db, map);
			db.close();
		}
	}

	// removes all locally stored data
	private void removeAllLocally(Context ctx, SharedPreferences prefs) {
		DatabaseHelper dbHelper;
		SQLiteDatabase db;
		if (ctx != null) {
			dbHelper = new DatabaseHelper(ctx);

			api.clear();
			items.clear();
			details.clear();
			eAdapter.notifyDataSetChanged();
			ImageLoader.getInstance().clearDiscCache();
			ImageLoader.getInstance().clearMemoryCache();

			Editor edit = prefs.edit();
			edit.putBoolean("pref_clear_cache", false);
			edit.commit();
			db = dbHelper.getWritableDatabase();
			LocalResultsProvider.deleteAllFromLocal(ctx, db);
			db.close();
		}
	}

	// called when user change language/hub from menu
	private void resetView() {
		api.clear();
		items.clear();
		details.clear();
		eAdapter.notifyDataSetChanged();
	}

}
