package pl.michalu.wikialistview.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Class to manage action related to Network connection
 * 
 * @author michalu
 * 
 */
public class NetworkManager {

	// checking state of network connection
	public static boolean isOnline(Context ctx) {
		if (ctx == null)
			return false;
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	// downloads data from given url and returns it as a string
	public static String downloadUrl(Context ctx, String url) throws IOException {

		if (!NetworkManager.isOnline(ctx))
			return null;
		InputStream is = null;
		try {
			URL address = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) address.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			is = conn.getInputStream();

			// Convert the InputStream into a string
			String contentAsString = readIt(is);
			return contentAsString;

			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	// reads data from inputstream and returns as a stream
	public static String readIt(InputStream stream) throws IOException, UnsupportedEncodingException {
		BufferedReader reader = null;
		StringBuilder inputStringBuilder = new StringBuilder();
		reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));

		String line = reader.readLine();
		while (line != null) {
			inputStringBuilder.append(line);
			inputStringBuilder.append('\n');
			line = reader.readLine();
		}
		return inputStringBuilder.toString();
	}
}
