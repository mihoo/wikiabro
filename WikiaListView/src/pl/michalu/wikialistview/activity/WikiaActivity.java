package pl.michalu.wikialistview.activity;

import pl.michalu.wikialistview.R;
import pl.michalu.wikialistview.fragment.WikisList;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

public class WikiaActivity extends ActionBarActivity {
	private static final String TAG = "WikisList";
	private WikisList wikisList;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wikia_activity);
		actionBar = getSupportActionBar();

		FragmentManager manager = getSupportFragmentManager();
		wikisList = (WikisList) manager.findFragmentByTag(TAG);
		if (wikisList == null) {
			wikisList = WikisList.create(R.id.wikis_list_view);
			FragmentTransaction ft = manager.beginTransaction();
			ft.replace(R.id.wikis_list_view, wikisList, TAG).commit();
			manager.executePendingTransactions();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

}
